<?php
/*
Plugin Name: WooCommerce Enroll Products
Description: Use this plugin to change the "Add to Cart" button text to "Enroll Now"
*/
class WooCommerceEnrollProducts {
	function __construct() {
		add_filter( 'single_add_to_cart_text', array($this, 'woo_custom_cart_button_text') );
	}
	function woo_custom_cart_button_text() {
		return __('Enroll Now', 'woocommerce');
    } 
}
$WooCommerceEnrollProducts = new WooCommerceEnrollProducts();
?>